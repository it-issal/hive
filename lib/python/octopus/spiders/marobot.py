# -*- coding: utf-8 -*-

from octopus.shortcuts import *

class MarobotSpider(CrawlSpider):
    name = "marobot"
    allowed_domains = ["store.marobot.com"]

    start_urls = (
        'http://store.marobot.com/',
    )

    rules = (
        Rule(LinkExtractor(allow=('index\.php\/([\w\d\-\_\/]+)\.html', )),             callback='parse_article', follow=True),
    )

    def parse_article(self, response):
        nomenclature = [
            x
            for x in ' '.join(response.xpath('/'.join([
                '/', 'div[@class="col-main"]', 'div[starts-with(@class,"page-title")]', '@class'
            ])).extract()).replace('-title','').split(' ')
            if x not in ('page',)
        ]

        if 'category' in nomenclature:
            categ = RetailCategory()

            categ['site']       = self.name
            categ['link']       = response.url
            categ['uid']        = cleanup(response.xpath('//div[@class="col-main"]/div[starts-with(@class,"page-title")]/h1/text()'), sep=None, trim=True)

            categ['title']      = cleanup(response.xpath('//div[@class="col-main"]/div[starts-with(@class,"page-title")]/h1/text()'), sep=None, trim=True)

            #*****************************************************************************************************************

            categ['products']   = [
                {
                    'uid':   cleanup(product.xpath('.//h1/a/@href'), sep=None, trim=True, inline=True),
                    'title': cleanup(product.xpath('.//h1/a/text()'), sep=None, trim=True, inline=True),
                    'url':   cleanup(product.xpath('.//h1/a/@href'), sep=None, trim=True, inline=True),
                    'thumb': cleanup(product.xpath('.//img/@src'), sep=None, trim=True, inline=True),
                }
                for product in response.xpath('//*[@class="product-view"]')
            ]

            #*****************************************************************************************************************

            #categ['products']   = response.xpath('').extract()

            yield categ
        else:
            entry = RetailArticle()

            entry['link']        = response.url
            entry['site']        = self.name
            entry['uid']         = cleanup(response.xpath('//*[@class="no-display"]/input[@name="product"]/@value'), sep=None, trim=True)

            entry['vendor']      = {
                'specs': dict([
                    tuple([
                        ' '.join(x.xpath('.//text()').extract()).replace('.', ';')
                        for x in row.xpath('.//td')
                    ])
                    for row in response.xpath('//*[@class="product-collateral"]//*[@class="std"]//table//tr')
                    if len(row.xpath('.//td'))==2
                ]),
            }

            entry['title']       = cleanup(response.xpath('//*[@class="product-name"]/h1/text()'), sep=None, trim=True)
            entry['summary']     = cleanup(response.xpath('//*[@class="short-description"]/text()'), sep='\n', trim=True)
            entry['description'] = cleanup(response.xpath('//*[@class="product-collateral"]//*[@class="std"]/p/text()'), sep='\n', trim=True)

            entry['pricing']         = dict(
                per_unit = dict(
                    devise = 'MAD',
                    value  = cleanup(response.xpath('//span[@class="price"]/text()'), sep=None, trim=True).replace('DH','').strip(),
                ),
            )

            gallery = [
                link
                for link in response.xpath('//*[@class="product-view"]//img/@src').extract()
                if ('skin/frontend/default' not in link)
            ]

            entry['media'] = {
                'links':      [ link for link in gallery if 'thumbnail' not in link ],
                'thumbnails': [ link for link in gallery if 'thumbnail' in link ],
            }

            yield entry
