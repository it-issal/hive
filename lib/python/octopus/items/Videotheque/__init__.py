# -*- coding: utf-8 -*-

from octopus.utils import *

#****************************************************************************************

class WebTalk(NativeItem):
    site         = scrapy.Field()
    uid          = scrapy.Field()
    slug         = scrapy.Field()

    title        = scrapy.Field()
    summary      = scrapy.Field()
    description  = scrapy.Field()

    when         = scrapy.Field()
    link         = scrapy.Field()
    rating       = scrapy.Field()
    views        = scrapy.Field()

    event        = scrapy.Field(default={})
    presenters   = scrapy.Field(default=[])
    license      = scrapy.Field()

    provider     = scrapy.Field()
    narrow       = scrapy.Field()

    media        = scrapy.Field(default={})

    #**********************************************************************************************************************

    def cleanup(self):
        return self

        #*****************************************************************************************************************

        raw = self['uid']

        self['uid'] = ''

        for c in raw:
            try:
                x = int(c)

                self['uid'] += c
            except:
                pass

        try:
            self['uid'] = int(self['uid'])
        except ValueError,ex:
            pass

        #*****************************************************************************************************************

        for key in self['pricing']:
            if len(self['pricing'][key]):
                if self['pricing'][key][0] in DEVISEs:
                    self['pricing'][key] = dict(
                        devise = self['pricing'][key][0],
                        value  = float(self['pricing'][key][1:]),
                    )

        #*****************************************************************************************************************

        return self

    #######################################################################################################################

    class Validator:
        def check(self):
            pass # raise DropItem("Article is not validated !")

    #######################################################################################################################

    class Schema:
        collection = 'web_talks_videos'
        unique_key = [
            ('site',  pymongo.ASCENDING),
            ('slug',  pymongo.ASCENDING),
        ]
