# -*- coding: utf-8 -*-

import scrapy

from scrapy.exceptions import DropItem

from octopus.shortcuts import *

#****************************************************************************

class ValidationPipeline(NativePipeline):
    VALIDATORS = {
        #RetailArticle: ArticleValidator,
    }
    
    def process_item(self, item, spider):
        if ('link' in item) and ('narrow' in item):
            item['link'] += '#' + item['narrow']
        
        if type(item) in self.VALIDATORs:
            hnd = self.VALIDATORs[item]
            
            cnt = hnd(self, item, spider)
            
            try:
                cnt.validate()
            except DropItem,ex:
                raise ex
        
        return item

#****************************************************************************

class CleanupPipeline(NativePipeline):
    def process_item(self, item, spider):
        if hasattr(item, 'cleanup'):
            if callable(item.cleanup):
                item.cleanup()
            
            def filter_sub(entry):
                if 'url' in entry:
                    entry['url'] = urljoin(spider.start_urls[0], entry['url'])
                
                return entry
            
            if 'products' in item:
                item['products'] = [
                    sub
                    for sub in item.get('products', [])
                ]
        
        return item

#****************************************************************************

class EnrichPipeline(NativePipeline):
    def process_item(self, item, spider):
        return item

