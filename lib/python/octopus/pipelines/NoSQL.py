# -*- coding: utf-8 -*-

import scrapy

from scrapy.exceptions import DropItem

import datetime

from urlparse import urlparse

from pymongo import errors
from pymongo.mongo_client import MongoClient
from pymongo.mongo_replica_set_client import MongoReplicaSetClient
from pymongo.read_preferences import ReadPreference

from scrapy import log

from octopus.utils import NativeItem

#****************************************************************************

import scrapycouchdb

class CouchDBPipeline(scrapycouchdb.CouchDBPipeline):
    pass

#****************************************************************************

VERSION = '0.7.2'

def not_set(string):
    """ Check if a string is None or ''

    :returns: bool - True if the string is empty
    """
    if string is None:
        return True
    elif string == '':
        return True
    return False

class MongoDBPipeline():
    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler)

    def __init__(self, crawler):
        from octopus import settings

        self.config = dict([
            (key, getattr(settings, 'MONGODB_%s' % var, None) or default)
            for key,var,default in (
                ('target',   'URI',                'mongodb://localhost:27017'),
                ('database', 'DATABASE',           'octopus'),
                ('gabarit',  'DEFAULT_COLLECTION', 'scrapy-item'),
                ('mapping',  'REMAP_COLLECTION',   '%s'),
                ('fsync',    'FILE_SYNC',          False),
                ('wconcern', 'WRITE_CONCERN',      0),
                ('buffer',   'BUFFER',             None),
                ('add_ts',   'ADD_TIMESTAMP',      False),
                ('halt_dup', 'HALT_ON_DUPLICATES', False),
            )
        ])

        self.crawler = crawler

        self.link = urlparse(self.config['target'])

        self.conn = MongoClient(self.config['target'],
            fsync           = self.config['fsync'],
            read_preference = ReadPreference.PRIMARY,
        )

        self.db = self.conn[self.config['database']]

        log.msg('Connected to MongoDB {0}, using database "{1}"'.format(
            self.link,
            self.config['database'],
        ))

    def __getitem__(self, obj):
        resp = obj

        if issubclass(type(obj), NativeItem):
            resp = object()

        if not hasattr(resp, 'collection'):
            setattr(resp, 'collection', 'scrapy-item')

            if issubclass(type(obj), NativeItem):
                resp.collection = obj.__class__.__name__.lower()

        resp.collection = self.config['mapping'] % resp.collection

        if not hasattr(resp, 'unique_key'):
            setattr(resp, 'bridge', None)

        if not hasattr(resp, 'bridge'):
            setattr(resp, 'bridge', self.db[resp.collection])

        if not hasattr(resp, 'counter'):
            setattr(resp, 'counter', 0)

        #if key not in self.cache:
        #    self.cache[key] = None

        return obj

    def process_item(self, item, spider):
        idx = self[getattr(item, 'Schema', None)]

        #item.MongoDB = idx

        # Ensure unique index
        if idx.unique_key:
            idx.bridge.ensure_index(idx.unique_key, unique=True)

            #log.msg("Ensuring index for key '{0}'".format(idx.unique_key))

        # Get the duplicate on key option
        if self.config['halt_dup']:
            tmpValue = self.config['halt_dup']
            if tmpValue < 0:
                log.msg(
                    (
                        'Negative values are not allowed for'
                        ' MONGODB_STOP_ON_DUPLICATE option.'
                    ),
                    level=log.ERROR
                )
                raise SyntaxError(
                    (
                        'Negative values are not allowed for'
                        ' MONGODB_STOP_ON_DUPLICATE option.'
                    )
                )
            self.stop_on_duplicate = self.config['halt_dup']
        else:
            self.stop_on_duplicate = 0

        return self.insert_item(idx, item, spider)

    def close_spider(self, spider):
        pass

    def insert_item(self, idx, item, spider):
        doc = dict([
            (k, item[k])
            for k in item
        ])

        if self.config['add_ts']:
            item[self.config['add_ts']] = datetime.datetime.utcnow()

        lst = getattr(idx, 'mapping', None)

        if type(lst) is dict:
            for key,hnd in lst.iteritems():
                if (key in doc):
                    if callable(hnd):
                        doc[key] = hnd(doc, key)
                    elif hnd in doc:
                        doc[key] = doc[hnd]

                        del doc[hnd]
                    else:
                        del doc[key]

        if not idx.unique_key:
            try:
                idx.bridge.insert(doc, continue_on_error=True)

                log.msg('Stored item(s) in MongoDB {0}/{1}'.format(self.config['database'], idx.collection),
                    level=log.DEBUG,
                    spider=spider,
                )
            except errors.DuplicateKeyError:
                log.msg('Duplicate key found', level=log.DEBUG)
                if (self.stop_on_duplicate > 0):
                    self.duplicate_key_count += 1
                    if (self.duplicate_key_count >= self.stop_on_duplicate):
                        self.crawler.engine.close_spider(
                            spider,
                            'Number of duplicate key insertion exceeded'
                        )
                pass

        else:
            key = {}

            if isinstance(idx.unique_key, list):
                for k in dict(idx.unique_key).keys():
                    key[k] = item[k]
            else:
                key[idx.unique_key] = item[idx.unique_key]

            idx.bridge.update(key, doc, upsert=True)

            log.msg('Stored item(s) in MongoDB {0}/{1}'.format(self.config['database'], idx.collection),
                level=log.DEBUG,
                spider=spider,
            )

        setattr(item, '_mongo', doc)

        return item
