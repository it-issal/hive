#!/bin/bash

export PREVIOUS_LOCATION=$DIRSTACK

cd ~/hive/mongos

mongodump -u mist -p 0f377f95486d4aa0bf6734b72c36a0d0 --host ds047720.mongolab.com --port 47720 --db octopus -vvvvv
mongodump -u mist -p 0f377f95486d4aa0bf6734b72c36a0d0 --host ds035300.mongolab.com --port 35300 --db vortex  -vvvvv

cd $PREVIOUS_LOCATION

unset PREVIOUS_LOCATION
