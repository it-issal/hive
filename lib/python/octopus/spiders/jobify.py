# -*- coding: utf-8 -*-

from octopus.shortcuts import *

from urlparse import urlparse, parse_qs as urlparse_qs

class JobifySpider(CrawlSpider):
    name = "jobify"
    allowed_domains = ["jobify.ma", "www.jobify.ma"]
    
    start_urls = (
        'http://www.jobify.ma/',
    )
    
    rules = (
        Rule(LinkExtractor(allow=('search', )), callback='parse_offer', follow=True),
    )
    
    def parse_category(self, response):
        pass
    
    def parse_offer(self, response):
        for entry in response.xpath('//div[@class="widget widgetOfferDetail"]'):
            offer = JobOffer()
            
            uri = urlparse(response.url)
            
            offer['link']          = response.url
            offer['source']        = self.name
            offer['narrow']        = urlparse_qs(uri.query).get('offerID', [''])[0]
            
            if len(offer['narrow']):
                offer['title']         = cleanup(entry.xpath('.//div[1]/h6/a/text()'), sep=None, trim=True)
                offer['summary']       = cleanup(entry.xpath('.//span[@class="offerDesc"][3]/text()'), sep=None, trim=True)
                #offer['when']          = cleanup(entry.xpath('.//div[@id="view-right"]/span/text()'), sep=None, trim=True)
                
                offer['company']       = {
                    'name':    cleanup(entry.xpath('.//span[@class="companyName"]/text()'), sep=None, trim=True),
                    'summary': cleanup(entry.xpath('.//span[@class="offerDesc"][2]/text()'), sep=None, trim=True),
                    'location': cleanup(entry.xpath('.//span/strong/text()'), sep=None, trim=True),
                }
                
                offer['details'] = {
                    'seats':    cleanup(entry.xpath('.//span[@class="offerDesc"][1]/text()'), sep=None, trim=True),
                    'profile':  cleanup(entry.xpath('.//span[@class="offerDesc"][4]/text()'), sep=None, trim=True),
                    'contract': cleanup(entry.xpath('.//li[@class="bottom"]/span[@class="dataNumGrey"]/text()'), sep=None, trim=True),
                    'salary':   cleanup(entry.xpath('.//li[@class="bottom"]/span[@class="dataNumRed"]/text()'), sep=None, trim=True),
                }
                
                offer['details']['salary'] = offer['details']['salary'].replace('Salaire : ', '').replace(' DH', '')
                
                yield offer

