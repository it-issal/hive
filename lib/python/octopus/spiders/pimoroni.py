# -*- coding: utf-8 -*-

from octopus.shortcuts import *

class PimoroniSpider(CrawlSpider):
    name = "pimoroni"
    allowed_domains = ["shop.pimoroni.com"]

    start_urls = (
        'https://shop.pimoroni.com/',
    )

    rules = (
        Rule(LinkExtractor(allow=('collections\/(.*)', )),                callback='parse_categ',   follow=True),
        Rule(LinkExtractor(allow=('collections\/(.*)/products\/(.*)', )), callback='parse_categ_product', follow=True),

        Rule(LinkExtractor(allow=('products\/(.*)', )),                   callback='parse_product', follow=True),
    )

    def parse_categ(self, response):
        categ = RetailCategory()

        categ['link'] = response.url
        categ['site'] = self.name
        categ['uid']  = urlparse(response.url).path.split('/')[2]

        categ['title']      = cleanup(response.xpath('//div[@id="collection"]/h2[1]/text()'), sep=None, trim=True)
        categ['title']     += cleanup(response.xpath('//div[@id="sub_collections"]/h2[1]/text()'), sep=None, trim=True)

        categ['products']   = [
            {
                #'vendor': cleanup(product.xpath('./@data-vendor'), sep=None, trim=True),
                'uid':    cleanup(product.xpath('./@data-id'), sep=None, trim=True),
                'title':  cleanup(product.xpath('./@data-title'), sep=None, trim=True),
                'url':    cleanup(product.xpath('./div[@class="col-md-3"]/a/@href'), sep=None, trim=True),
            }
            for product in response.xpath('//div[contains(@class, "product")]')
        ]

        yield categ

    def parse_categ_product(self, response):
        if False:
            yield None

    def parse_product(self, response):
        entry = RetailArticle()

        entry['link'] = response.url
        entry['site'] = self.name
        entry['uid']  = cleanup(response.xpath('//button[@class="add-to-cart-lg"]/@value'), sep=None, trim=True)

        entry['vendor'] = dict(
            name = cleanup(response.xpath('//div[@id="product"]//div[@itemprop="brand manufacturer"]/text()'), sep=None, trim=True),
        )

        entry['title']       = cleanup(response.xpath('//div[@id="title"]/h1//text()'), sep=None, trim=True)
        entry['description'] = cleanup(response.xpath('//div[@id="description"]/p/text()'), sep='\n', trim=True)

        entry['stock']       = {
            'state': cleanup(response.xpath('//strong[@class="stock-level"]//text()'), sep=None, trim=True).lower(),
        }

        if 'in stock' in entry['stock']['state']:
            entry['stock']['count'] = entry['stock']['state'].replace('in','').replace('stock','').strip()
            entry['stock']['state'] = 'availible'

        entry['pricing']         = dict(
            per_unit={
                'devise': 'GBP',
                'value':  cleanup(response.xpath('//div[@class="price"]/text()'), sep=None, trim=True).replace('£',''),
            },
        )

        entry['media'] = {
            'cover': cleanup(response.xpath('//div[@id="product"]//div[@itemprop="image"]/text()'), sep=None, trim=True),
            'links': response.xpath('//*[@id="gallery"]/a/img/@src').extract(),
        }

        if entry['uid']:
            yield entry
