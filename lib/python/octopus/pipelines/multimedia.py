# -*- coding: utf-8 -*-

import scrapy

from scrapy.contrib.pipeline.images import ImagesPipeline
from scrapy.exceptions import DropItem

#****************************************************************************

class IllustratorPipeline(ImagesPipeline):
    def get_media_requests(self, item, info):
        if hasattr(item, 'filter_image'):
            for link in item['media'].get('links', []):
                if link.startswith('//'):
                    link = 'http:' + link

                yield scrapy.Request(link)

    def item_completed(self, results, item, info):
        if hasattr(item, 'filter_image'):
            resp = [
                entry
                for kv in results
                for entry in item.filter_image(*kv)
            ]

            if not resp:
                pass # raise DropItem("Item contains no images")
            else:
                item['media']['paths'] = resp

        return item
