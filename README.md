Shell commands :
================

* hive
* bee

* nebula
* jobctl
* etlctl

Features :
==========

* Hosting of Nebula masters & redunduncy nodes.
* Hosting of reflective APIs upon storage definitions & features (SPARQL / Indexation / Analytics / Mining / B.I).

* Task handlers for Voodoo particles.
* ETL jobs for Hive particles.
* Build jobs via BuildBOt for Deming particles.

Debian packages :
=================

# Libraries : #
----------------

* behat

* RDFlib

# Connectors : #
----------------

* odbc
* jdbc

Docker :: Backends :
====================

# Cache : #
-----------

* Memcache
* Redis

# SQL : #
---------

* SQLite3
* MySQL
* PostgreSQL

* MariaDB
* Drizzle

# Key-Value : #
---------------

* memcached
* redis
* apache-cassandra

# Document : #
--------------

* cockroach
* couchbase
* mongodb
* rethinkdb

# NoSQL : #
-----------

* elasticsearch

# Graph : #
-----------

* allegrograph
* neo4j
* rexter-titan

# Semantic : #
--------------

* apache-marmotta
* apache-stanbol

Docker :: Backends :
====================

# C.I : #
---------

* gitlab

* behat
* buildbot
* jenkins
* selenium

# Office : #
------------

* openkm
* odoo
* owncloud
