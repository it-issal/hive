# -*- coding: utf-8 -*-

from octopus.shortcuts import *

class AlibabaSpider(CrawlSpider):
    name = "alibaba"
    allowed_domains = ["alibaba.com", "www.alibaba.com"]
    
    start_urls = (
        'http://www.alibaba.com/wholesale',
    )
    
    rules = (
        Rule(LinkExtractor(allow=('wholesale/categories/([\-\_\.\/\w\d]*)_([\d]+)\.html', )), callback='parse_category', follow=True),
        Rule(LinkExtractor(allow=('product-detail/([\-\_\.\/\w\d]*)_([\d]+)\.html', )), callback='parse_produit', follow=True),
    )
    
    GET_UUID = re.compile('(.*)_([\d]+)\.html$')
    
    def parse_category(self, response):
        pass
    
    def parse_produit(self, response):
        entry = RetailArticle()
        
        entry['link']        = response.url
        entry['site']        = self.name
        entry['categories']  = response.xpath('//div[@class="ui-breadcrumb"]/a[@class="category"]/@href').extract()
        
        m = self.GET_UUID.match(response.url)
        
        if m:
            entry['uid'] = int(m.groups()[1])
        
        entry['title']       = cleanup(response.xpath('//div[@class="title"]//h1[@class="fn"]/text()'), sep=None, trim=True)
        entry['summary']     = cleanup(response.xpath('//div[@class="box"]/p[@class="description"]/text()'), sep='\n', trim=True)
        entry['description'] = cleanup(response.xpath('//div[@class="box"]/div[starts-with(@class,"richtext")]/*'), sep='\n', trim=True)
        
        for node in response.xpath('//table[@class="btable"]//tr'):
            if 'Min. Order' in cleanup(node.xpath('.//td[@class="name"]/text()'), sep=None, trim=True):
                entry['min_unit']     = cleanup(node.xpath('.//td[@class="value"]/text()'), sep=None, trim=True)
        
        entry['shipping']['qte_unit'] = cleanup(response.xpath('//span[@class="measurement"]/text()'), sep=None, trim=True)
        entry['pricing']['unit']      = cleanup(response.xpath('//*[@class="price"][1]//text()[1]'), sep=None, trim=True)
        
        entry['shipping']['min_unit'] = entry.get('min_unit', 'unknown').replace('Pieces', '').strip()
        entry['shipping']['qte_unit'] = entry['qte_unit'].replace('/', '').strip()
        entry['pricing']['ht']        = entry['price_unit'].replace('US $', '')
        
        entry['media']['links']  = response.xpath('//img[@data-role="thumb"]/@src').extract()
        
        return entry

