# -*- coding: utf-8 -*-

from octopus.utils import *

#****************************************************************************************

class JobOffer(NativeItem):
    source       = scrapy.Field()
    narrow       = scrapy.Field()

    title        = scrapy.Field()
    summary      = scrapy.Field()
    when         = scrapy.Field()

    company      = scrapy.Field(default={})
    details      = scrapy.Field(default={})

    #**********************************************************************************************************************

    def cleanup(self):
        return self

        #*****************************************************************************************************************

        raw = self['uid']

        self['uid'] = ''

        for c in raw:
            try:
                x = int(c)

                self['uid'] += c
            except:
                pass

        try:
            self['uid'] = int(self['uid'])
        except ValueError,ex:
            pass

        #*****************************************************************************************************************

        for key in self['pricing']:
            if len(self['pricing'][key]):
                if self['pricing'][key][0] in DEVISEs:
                    self['pricing'][key] = dict(
                        devise = self['pricing'][key][0],
                        value  = float(self['pricing'][key][1:]),
                    )

        #*****************************************************************************************************************

        return self

    #######################################################################################################################

    class Validator:
        def check(self):
            pass # raise DropItem("Article is not validated !")

    #######################################################################################################################

    class Schema:
        collection = 'job_offer'
        unique_key = [
            ('source', pymongo.ASCENDING),
            ('narrow', pymongo.ASCENDING),
        ]
