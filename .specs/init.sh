#!/bin/bash

schema="apache" ; for key in malamute stanbol ; do if [[ ! -d opt/$schema/$key ]] ; then git submodule add git@bitbucket.org:docker-warrior/$schema"-"$key.git opt/$schema/$key ; fi ; done

schema="code" ; for key in gerrit gitlab jenkins-ci selenium sonatype-nexus strider-cd ; do if [[ ! -d usr/$schema/$key ]] ; then git submodule add git@bitbucket.org:docker-warrior/$key.git usr/$schema/$key ; fi ; done
schema="platform" ; for key in shibboleth ; do if [[ ! -d usr/$schema/$key ]] ; then git submodule add -f git@bitbucket.org:docker-warrior/$key.git usr/$schema/$key ; fi ; done
schema="business" ; for key in freepbx opengts openkm owncloud ; do if [[ ! -d usr/$schema/$key ]] ; then git submodule add git@bitbucket.org:docker-warrior/$key.git usr/$schema/$key ; fi ; done
schema="daemon" ; for key in asterisk collectd hostap openvpn traccar nginx haproxy ; do if [[ ! -d usr/$schema/$key ]] ; then git submodule add git@bitbucket.org:docker-warrior/$key.git usr/$schema/$key ; fi ; done
schema="hosting" ; for key in samza-mesos ; do if [[ ! -d usr/$schema/$key ]] ; then git submodule add git@bitbucket.org:docker-warrior/$key.git usr/$schema/$key ; fi ; done
schema="panel" ; for key in docker-ui php{Redis,My}Admin ; do if [[ ! -d usr/$schema/$key ]] ; then git submodule add git@bitbucket.org:docker-warrior/$key.git usr/$schema/$key ; fi ; done
schema="data" ; for key in allegrograph cockroachdb couchbase elasticsearch gaffer memcached mongodb mysql neo4j postgres rabbitmq redis rethinkdb rexter-titan virtuoso zeromq ; do if [[ ! -d usr/$schema/$key ]] ; then git submodule add git@bitbucket.org:docker-warrior/$key.git usr/$schema/$key ; fi ; done
